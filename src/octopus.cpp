#include <iostream>
#include <filesystem>
#include "cli_base.hpp"

using namespace octopus;
namespace fs = std::filesystem;

// Default values of CLI and config file
namespace octopus::Defaults {
	// Actual value should look like this, but for the sake of debugging take pwd
//	const fs::path octopus_root = "/usr/local";
	const fs::path octopus_root = "./";
	// Where to search for plugins
	const std::list<fs::path> plugin_paths = {octopus_root, "./octopus_plugins"};
}

int main( const int argc, const char* argv[] ) {
	// Main cli object
	CLI::App app{"Main CLI for octopus program", "octopus++"};

	// Global options
	fs::path octopus_root;
	std::list<fs::path> plugin_paths;
	bool ignore_plugin_path;

	// Configure octopus++ cli options
	app.set_config("--config", "octopus.conf",
	               "Override system default configurations", true)->
			// Make sure it is a valid file, otherwise use Defaults::octopus_root
			transform(CLI::FileOnDefaultPath(Defaults::octopus_root));
	app.add_option("--octopus-root", octopus_root,
	               "Root directory of the octopus installation")->
			// Can pass as environment variable as well
			envname("OCTOPUS_ROOT")->
			// Use a default
			default_val(Defaults::octopus_root)->
			// Another way to check if file exists
			check(CLI::ExistingPath);
	app.add_option("--plugin_paths", plugin_paths,
	               "Search path for loading plugins")->
			// Ok, now you know what these do
			default_val(Defaults::plugin_paths)->
			check(CLI::ExistingPath);
	// Just for debugging, we can bypass the plugins loading. Maybe it can work with LD_LIBRARY_PATH?
	app.add_flag("--ignore-path",ignore_plugin_path,
				 "Ignore plugin_paths");
	app.set_version_flag("--version","0.1");


	// Remaining cli logic

	// Pass at most one subcommand
	app.require_subcommand(-1);

	// Add octopus_root to plugin_paths if it was not included
	if (std::none_of(plugin_paths.begin(), plugin_paths.end(),
	                 [octopus_root]( auto& it ) { return it == octopus_root; }))
		plugin_paths.push_front(octopus_root);

	// Quick parse cli to get the correct values
	try {
		app.parse(argc, argv);
	} catch (const CLI::CallForHelp& e){

	} catch (const CLI::ParseError& e) {
		return app.exit(e);
	}

	if (!ignore_plugin_path) {
		// Load all plugins
		for (const auto& root_path: plugin_paths) {
			// Skip if path is not a directory
			if (!exists(root_path) || !is_directory(root_path))
				continue;
			// Search all files and folders recursively
			for (auto iter = fs::recursive_directory_iterator{root_path};
			     iter != fs::recursive_directory_iterator();
			     ++iter) {
				auto& path = iter->path();
				if (iter.depth() == 0 && is_directory(path) && path.filename() != "lib")
					// Only search recursively in directories named lib
					iter.disable_recursion_pending();
				else if (is_regular_file(path) && path.extension() == ".so" && path.stem() != "libcli_base")
					// Load any so library that is not libcli_base.so
					dLibrary_t::Create(absolute(path));
			}
		}
	}

	// Add all sub-commands that have registered themselves
	for (auto& subCommand: cli::SubCommands()) {
		app.add_subcommand(subCommand);
	}

	// Perform the CLI
	try {
		app.parse(argc, argv);
	} catch (const CLI::ParseError& e) {
		return app.exit(e);
	}
	return 0;
}
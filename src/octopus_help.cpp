#include <iostream>

#include "octopus_help.hpp"

using namespace octopus::cli;

Help::Help() :
		base_cli<Help>("Display helpful information about octopus setup", "help") {
	// Link --search to Search()
	add_option_function<std::string>("-s,--search", [this]( const std::string& var ) {
		if (var.empty())
			// If nothing is passed, just call List()
			List();
		else
			Search(var);
	}, "Search variables whose names contain string 'STRING'.");
	// Link --print to Print()
	add_option_function<std::string>("-p,--print", [this]( const std::string& var ) {
		Print(var);
	}, "Prints description of variable 'VARNAME'.");
	// Link --list to List()
	add_flag_callback("-l,--list", [this]() {
		List();
	}, "Lists all variables.");
	// Allow passing only one option
	require_option(1);
}

void Help::Search( std::string var ) {
	std::cout << "Insert actual implementation here: " << var << std::endl;
}
void Help::Print( std::string var ) {
	std::cout << "Insert actual implementation here: " << var << std::endl;
}
void Help::List() {
	std::cout << "Insert actual implementation here " << std::endl;

}

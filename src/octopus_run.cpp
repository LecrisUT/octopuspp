#include <iostream>

#include "octopus_run.hpp"

using namespace octopus::cli;

Run::Run() :
		base_cli<Run>("Run the main octopus program", "run") {
	// Hopefully can read what these options do
	add_option("-i,--input", inp_file, "Some random text")->
			default_val("inp")->
			check(CLI::ExistingFile);
	final_callback([this]() { run_octopus(); });
}
void Run::run_octopus() {
	std::cout << "Pretend we are running octopus here now" << std::endl;
}

#ifndef OCTOPUS_OCTOPUS_BASE_HPP
#define OCTOPUS_OCTOPUS_BASE_HPP

#include <CLI/App.hpp>
#include <CLI/Config.hpp>
#include <CLI/Formatter.hpp>

#include <list>
#include <memory>

namespace octopus {
	namespace cli {
		/**
		 * Global list of subcommands register themselves
		 *
		 * @return List of cli subcommands (shared pointers)
		 */
		std::list<CLI::App_p>& SubCommands();

		/**
		 * Class where the actual sub-commands are defined in
		 *
		 * @tparam T Derived class of the cli
		 */
		template<class T>
		class base_cli : public CLI::App {
		protected:
			/**
			 * Need a constructor of course
			 *
			 * @param description Description text of subcommand
			 * @param name Name of subcommand
			 */
			base_cli( std::string description, std::string name );
			/**
			 * Get the pointer to the subcommand or create it if it doesn't exist
			 *
			 * @return pointer to the subcommand object
			 */
			static std::shared_ptr<T> GetOrCreate();
			/**
			 * Pre-defined location where the subcommand exists
			 */
			static std::shared_ptr<T> Instance;
			// Make sure that an instance exists
			static_assert(&Instance);
		};
	}

	// Wrapper around dlopen because this is C++20 not C98
	class dLibrary_t;
	using DLibrary = std::shared_ptr<dLibrary_t>;
	std::list<DLibrary>& DLibraries();
	class dLibrary_t {
	public:
		static std::shared_ptr<dLibrary_t> Create( std::string_view libray );
		static std::shared_ptr<dLibrary_t> Create( const std::filesystem::path& libray );

		static void CloseDL( void* ptr );
		using ptr_type = std::unique_ptr<void, decltype(&CloseDL)>;
		static ptr_type OpenDL( const std::filesystem::path& location );
		const std::filesystem::path location;
		const ptr_type dll;
	private:
		explicit dLibrary_t( const std::filesystem::path& libray  );
		explicit dLibrary_t( std::string_view library );
	};

	// Helper functions for comparing the library wit the path it was built with
	bool operator==(const DLibrary& dlib, std::string_view path);
	bool operator==(const DLibrary& dlib, const std::filesystem::path& path);
	std::strong_ordering operator<=>(const DLibrary& dlib, std::string_view path);
	std::strong_ordering operator<=>(const DLibrary& dlib, const std::filesystem::path& path);
}

// Template function definitions needed in order to instantiate. Will be cleaner with
// C++20 modules
template<class T>
octopus::cli::base_cli<T>::base_cli( std::string description, std::string name ):
		CLI::App(description, name) { }

// Instance is always created using GetOrCreate. The user does not need to define or
// instantiate this manually. Works because of the static_assert. Template magic ;)
template<class T>
std::shared_ptr<T> octopus::cli::base_cli<T>::Instance = base_cli<T>::GetOrCreate();

template<class T>
std::shared_ptr<T> octopus::cli::base_cli<T>::GetOrCreate() {
	// Search for the sub-command in the static list we had before
	auto& subCommands = SubCommands();
	// Find if any subcommand is of the type T (the actual class of sub command)
	auto instance = std::ranges::find_if(subCommands,
	                                     []( auto& it ) { return dynamic_cast<T*>(it.get()) != nullptr; });
	if (instance == subCommands.end()) {
		// If there were no existing references, make a new one
		auto new_instance = std::make_shared<T>();
		subCommands.push_back(new_instance);
		return new_instance;
	}
	return std::dynamic_pointer_cast<T>(*instance);
}

#endif //OCTOPUS_OCTOPUS_BASE_HPP

#ifndef OCTOPUS_OCTOPUS_HELP_HPP
#define OCTOPUS_OCTOPUS_HELP_HPP

#include "cli_base.hpp"

namespace octopus::cli {
	/**
	 * Example implementation mimicking octopus-help
	 *
	 * See the constructor for how to link/call each method
	 */
	class Help :
			public base_cli<Help> {
	public:
		Help();
		void Search(std::string var);
		void Print(std::string var);
		void List();
	};
}

#endif //OCTOPUS_OCTOPUS_HELP_HPP

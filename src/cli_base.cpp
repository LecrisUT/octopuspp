#include <dlfcn.h>
#include <cassert>
#include "cli_base.hpp"

using namespace octopus;

std::list<CLI::App_p>& cli::SubCommands() {
	static std::list<CLI::App_p> subCommands = {};
	return subCommands;
}
std::list<DLibrary>& octopus::DLibraries() {
	static std::list<DLibrary> dlibraries = {};
	return dlibraries;
}
bool operator==( const DLibrary& dlib, std::string_view path ) {
	return dlib == std::filesystem::path(path);
}
bool octopus::operator==( const DLibrary& dlib, const std::filesystem::path& path ) {
	if (dlib == nullptr)
		return path.empty();
	return dlib->location == path;
}
std::strong_ordering operator<=>( const DLibrary& dlib, std::string_view path ) {
	return dlib <=> std::filesystem::path(path);
}
std::strong_ordering octopus::operator<=>( const DLibrary& dlib, const std::filesystem::path& path ) {
	if (dlib == nullptr)
		return "" <=> path;
	return dlib->location <=> path;
}

dLibrary_t::dLibrary_t( const std::filesystem::path& library ) :
		location(library), dll{OpenDL(location)} { }
dLibrary_t::dLibrary_t( std::string_view library ) : dLibrary_t(std::filesystem::path(library)) { }

std::shared_ptr<dLibrary_t> dLibrary_t::Create( const std::filesystem::path& library ) {
	auto& libs = DLibraries();
	auto instance = std::ranges::find_if(libs, [library](auto& it){return it == library;});
	if (instance == libs.end()) {
		auto new_instance = std::shared_ptr<dLibrary_t>(new dLibrary_t(library));
		libs.push_back(new_instance);
		return new_instance;
	}
	return *instance;
}
std::shared_ptr<dLibrary_t> dLibrary_t::Create( std::string_view libray ) {
	return Create(std::filesystem::path(libray));
}

dLibrary_t::ptr_type dLibrary_t::OpenDL( const std::filesystem::path& Location ) {
	void* dl = dlopen(Location.c_str(), RTLD_NOLOAD);
	assert(!dl);
	dl = dlopen(Location.c_str(), RTLD_NOW | RTLD_GLOBAL);
	assert(dl);
	return {dl, CloseDL};
}
void dLibrary_t::CloseDL( void* ptr ) {
	if (ptr == nullptr)
		dlclose(ptr);
}
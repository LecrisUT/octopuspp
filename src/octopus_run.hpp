#ifndef OCTOPUS_OCTOPUS_RUN_HPP
#define OCTOPUS_OCTOPUS_RUN_HPP

#include "cli_base.hpp"

namespace octopus::cli {
	/**
	 * Another example simulating octopus binary
	 */
	class Run :
			public base_cli<Run> {
	public:
		std::filesystem::path inp_file;
		Run();
		void run_octopus();
	};
}

#endif //OCTOPUS_OCTOPUS_RUN_HPP
